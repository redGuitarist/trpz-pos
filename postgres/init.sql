CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name varchar,
    login varchar UNIQUE,
    phone varchar UNIQUE,
    email varchar UNIQUE,
    position varchar,
    password_hash varchar,
    password_salt varchar
);

CREATE TABLE divisions (
    id SERIAL PRIMARY KEY,
    address varchar UNIQUE,
    head_id integer REFERENCES users(id)
);

CREATE TABLE tills (
    id SERIAL PRIMARY KEY,
    number integer,
    division_id integer REFERENCES divisions(id)
);

CREATE TABLE currencies (
    id SERIAL PRIMARY KEY,
    name varchar,
    code char(3) UNIQUE
);

CREATE TABLE availability (
    id SERIAL PRIMARY KEY,
    division_id integer REFERENCES divisions(id),
    currency_id integer REFERENCES currencies(id),
    amount real
    -- CONSTRAINT name_division_unique UNIQUE(id, name)
);

CREATE TABLE checks (
    id SERIAL PRIMARY KEY,
    exchange_id integer REFERENCES exchanges(id),
    text varchar
);

CREATE TABLE reports (
    id SERIAL PRIMARY KEY,
    division_id integer REFERENCES divisions(id),
    start_time timestamp,
    end_time timestamp,
    user_id integer REFERENCES users(id),
    content varchar
);

CREATE TABLE ratios (
    id SERIAL PRIMARY KEY,
    currency_buy integer REFERENCES currencies(id),
    currency_sell integer REFERENCES currencies(id),
    ratio real
);

CREATE TABLE exchanges (
    id serial PRIMARY KEY,
    time timestamp,
    currency_buy integer REFERENCES currencies(id),
    currency_sell integer REFERENCES currencies(id),
    client_amount real,
    till_id integer REFERENCES tills(id),
    user_id integer REFERENCES users(id)
);