#!/usr/bin/python3

import json
import datetime
import random
import string
import hashlib

from flask import Flask, render_template, request, redirect, make_response
from sqlalchemy.orm import sessionmaker
import redis
import sqlalchemy as sa

from classes import *

app = Flask(__name__)


def setup_db(sql_connstring, redis_opts):
    try:
        global db
        global rd
        global Session
        rd = redis.Redis(**redis_opts)
        db = sa.create_engine(sql_connstring, echo=True)
        Session = sessionmaker(bind=db)
        DBBase.metadata.create_all(db)
    except Exception:
        raise


def authorize(position=None):
    uname = request.cookies.get('login', None)
    division = request.cookies.get('division', None)
    till = request.cookies.get('till', None)
    if uname is None or division is None or till is None:
        return None

    s = Session()
    u = s.query(User).filter(User.login == uname).one()
    if position is None or u.position == position or u.position in position:
        return uname, division, till
    else:
        return None


setup_db('postgres://postgres:postgres@postgres/trpzpos', {'host': 'redis'})


def get_from_cache():
    # compose string
    string = request.path + \
        '&'.join(sorted([key+'='+value for key, value in request.values.items()]))
    print(string)
    
    # check in redis
    response = rd.get(string)
    return string, response


def add_to_cache(string, response):
    rd.set(string, response, ex=20)


@app.route('/api/get_currencies', methods=['GET'])
def get_currencies():
    string, response = get_from_cache()
    if response:
        return response

    s = Session()
    curs = s.query(Currency)
    rates = s.query(Ratio)

    ans = {'names': [], 'rates': []}
    for currency in curs:
        ans['names'].append(currency.to_json())

    for ratio in rates:
        code_buy = s.query(Currency).filter(
            Currency.id == ratio.currency_buy).one().code
        code_sell = s.query(Currency).filter(
            Currency.id == ratio.currency_sell).one().code
        ans['rates'].append(
            {'buy': code_buy, 'sell': code_sell, 'rate': ratio.ratio})

    response = json.dumps({'success': True, 'data': ans})
    add_to_cache(string, response)
    return response


@app.route('/api/set_exchange_rate', methods=['GET', 'POST'])
def set_exchange_rate():
    c = authorize('Admin')
    if not c:
        return json.dumps({'success': False, 'error': 'Unauthorized'})

    errorstr = ''
    if 'buy' not in request.values:
        errorstr = 'No currency to buy specified'
    elif 'sell' not in request.values:
        errorstr = 'No currency to sell specified'
    elif 'rate' not in request.values:
        errorstr = 'No exchange rate specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})

    s = Session()
    curr_buy = s.query(Currency).filter(
        Currency.code == request.values['buy']).one()
    print('Buy found')
    curr_sell = s.query(Currency).filter(
        Currency.code == request.values['sell']).one()
    print('Sell found')
    new_rate = s.query(Ratio).filter(
        Ratio.currency_buy == curr_buy.id).filter(Ratio.currency_sell == curr_sell.id).one()
    print('Rate found')
    new_rate.ratio = request.values['rate']
    s.add(new_rate)
    s.commit()

    return json.dumps({'success': True, 'data': None})
    # return redirect('/', code=302)


@app.route('/api/add_division')
def add_division():
    c = authorize()
    if not c:
        return json.dumps({'success': False, 'error': 'Unauthorized'})

    errorstr = ''
    if 'address' not in request.values:
        errorstr = 'No address specified'
    elif 'head_id' not in request.values:
        errorstr = 'No head id specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})

    d = Division(address=request.values['address'],
                 head_id=request.values['head_id'])
    s = Session()
    s.add(d)
    s.commit()
    return json.dumps({'success': True, 'data': d.to_json()})


@app.route('/api/add_till')
def add_till():
    c = authorize()
    if not c:
        return json.dumps({'success': False, 'error': 'Unauthorized'})

    errorstr = ''
    if 'division' not in request.values:
        errorstr = 'No division specified'
    elif 'num' not in request.values:
        errorstr = 'No number specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})

    t = Till(number=request.values['num'],
             division_id=request.values['division'])
    s = Session()
    s.add(t)
    s.commit()
    return json.dumps({'success': True, 'data': t.to_json()})


@app.route('/api/get_cash_left')
def get_cash_left():
    c = authorize()
    if not c:
        return json.dumps({'success': False, 'error': 'Unauthorized'})
    u, d, t = c

    string, response = get_from_cache()
    if response:
        return response

    s = Session()
    lefts = s.query(Availability).filter(Availability.division_id == d).all()
    data = []
    for curr_avail in lefts:
        curr = s.query(Currency).filter(
            Currency.id == curr_avail.currency_id).one()
        data.append(curr.to_json())
        data[-1]['amount'] = curr_avail.amount

    response = json.dumps({'success': True, 'data': data})
    add_to_cache(string, response)
    return response


@app.route('/api/get_exchanges', methods=['GET'])
def get_exchanges():
    string, response = get_from_cache()
    if response:
        return response

    offset = int(request.values.get('offset', 0))
    limit = int(request.values.get('limit', 25))
    s = Session()
    exchanges = s.query(Exchange).order_by(Exchange.time.desc()).limit(limit).offset(offset).all()
    data = []
    for exchange in exchanges:
        curr_buy = s.query(Currency).filter(
            Currency.id == exchange.currency_buy).one()
        curr_sell = s.query(Currency).filter(
            Currency.id == exchange.currency_sell).one()
        data.append(exchange.to_json())
        data[-1].update({'currency_buy': curr_buy.code,
                         'currency_sell': curr_sell.code})

    response = json.dumps({'success': True, 'data': data})
    add_to_cache(string, response)
    return response


@app.route('/api/add_exchange')
def add_exchange():
    c = authorize()
    if not c:
        return json.dumps({'success': False, 'error': 'Unauthorized'})
    user_id, division_id, till_id = c

    errorstr = ''
    if 'buy' not in request.values:
        errorstr = 'No currency to buy specified'
    elif 'sell' not in request.values:
        errorstr = 'No currency to sell specified'
    elif 'amount' not in request.values:
        errorstr = 'No client amount specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})

    s = Session()
    user_id = s.query(User).filter(User.login == user_id).one().id
    # find currencies, rate, availability
    curr_buy = s.query(Currency).filter(
        Currency.code == request.values['buy']).one()
    curr_sell = s.query(Currency).filter(
        Currency.code == request.values['sell']).one()
    ratio = s.query(Ratio).filter(Ratio.currency_buy == curr_buy.id).filter(
        Ratio.currency_sell == curr_sell.id).one()
    buy_avail = s.query(Availability).filter(
        Availability.division_id == division_id).filter(Availability.currency_id == curr_buy.id).one()
    sell_avail = s.query(Availability).filter(
        Availability.division_id == division_id).filter(Availability.currency_id == curr_sell.id).one()

    exchanger_amount = ratio.ratio * float(request.values['amount'])

    # check availability
    if sell_avail.amount < exchanger_amount:
        return json.dumps({'success': False, 'error': 'Not enough currency in the division'})

    sell_avail.amount -= exchanger_amount
    buy_avail.amount += float(request.values['amount'])

    e = Exchange(time=datetime.datetime.now(), currency_buy=curr_buy.id, exchanger_amount=exchanger_amount,
                 currency_sell=curr_sell.id, client_amount=request.values['amount'], till_id=till_id, user_id=user_id)
    s.add_all([e, sell_avail, buy_avail])
    s.commit()

    return json.dumps({'success': True, 'data': None})


@app.route('/api/get_divisions')
def get_divisions():
    string, response = get_from_cache()
    if response:
        return response

    s = Session()
    divisions = s.query(Division).all()

    request = json.dumps({'success': True, 'data': [division.to_json() for division in divisions]})
    add_to_cache(string, request)
    return response


@app.route('/api/get_tills')
def get_tills():
    string, response = get_from_cache()
    if response:
        return response

    errorstr = ''
    if 'division' not in request.values:
        errorstr = 'No division specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})
    s = Session()

    division = s.query(Division).filter(
        Division.id == int(request.values['division'])).one()

    response = json.dumps({'success': True, 'data': [till.to_json() for till in division.tills]})
    add_to_cache(string, response)
    return response


@app.route('/api/add_user')
def add_user():
    errorstr = ''
    if 'login' not in request.values:
        errorstr = 'No login specified'
    elif 'password' not in request.values:
        errorstr = 'No password specified'
    elif 'position' not in request.values:
        errorstr = 'No position specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})

    u = User()
    u.login = request.values['login']
    u.password_hash = request.values['password']
    u.position = request.values['position']
    u.name = request.values.get('name', u.login)
    u.phone = request.values.get('phone', '')
    u.email = request.values.get('email', u.login)
    u.password_salt = ''.join(random.choice(
        string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(10))

    u.password_hash += u.password_salt
    u.password_hash = hashlib.sha256(u.password_hash.encode()).hexdigest()

    s = Session()
    s.add(u)
    try:
        s.commit()
    except Exception:
        raise
        return json.dumps({'success': False, 'error': 'Cannot add user'})

    return json.dumps({'success': True, 'data': None})


@app.route('/api/login', methods=['GET', 'POST'])
def login():
    errorstr = ''
    if 'login' not in request.values:
        errorstr = 'No login specified'
    elif 'password' not in request.values:
        errorstr = 'No password specified'
    elif 'division' not in request.values:
        errorstr = 'No division specified'
    elif 'till' not in request.values:
        errorstr = 'No till specified'
    if errorstr:
        return json.dumps({'success': False, 'error': errorstr})

    s = Session()
    try:
        u = s.query(User).filter(User.login == request.values['login']).one()
        d = s.query(Division).filter(Division.id ==
                                     request.values['division']).one()
        t = s.query(Till).filter(Till.id == request.values['till']).one()
    except:
        return json.dumps({'success': False, 'error': 'Invalid username, division or till'})

    password_hash = hashlib.sha256(
        (request.values['password'] + u.password_salt).encode()).hexdigest()
    if password_hash == u.password_hash:
        resp = make_response(json.dumps(
            {'success': True, 'data': u.to_json()}))
        resp.set_cookie('login', value=u.login)
        resp.set_cookie('division', value=request.values['division'])
        resp.set_cookie('till', value=request.values['till'])
        return resp
    else:
        return json.dumps({'success': False, 'error': 'Invalid password'})


if __name__ == '__main__':
    app.run()
