# API docs

Every response comes in an envelope that looks like this:
```json
{
    "success": false, // or true
    "data": <actual response>, // absent if failure
    "error": "<human-readable error message>" // absent if ok
}
```
Root location is `/api`, so each method has a path like `/api/method`.


## get_currencies (GET)

Returns two lists: currencies with name and code, and all available exchange rates.

#### Params:

none

#### Response example:

```json
"names":
[
    {
        "id": 1,
        "name":"Доллар США",
        "code":"USD"
    },
    {
        "id": 2,
        "name":"Евро",
        "code":"EUR"
    }
],
"rates":
[
    {
        "buy":"EUR",
        "sell":"USD",
        "rate":"1.111"
    },
    {
        "buy":"USD",
        "sell":"EUR",
        "rate":"0.900"
    }
]
```

## set_exchange_rate (GET, POST)

Sets a new exchange rate for a given pair of currencies.

#### Params:

- `buy` - code of currency to buy
- `sell` - code of currency to sell
- `rate` - new exchange rate

#### Response example:

```json
null
```

## get_exchanges (GET)

Returns a list of exchanges.

#### Params:

- `limit` (optional, default 25) - return this many values in list
- `offset` (optioan, default 0) - return values starting from this (skip `offset` latest)

#### Response example:

```json
null
```