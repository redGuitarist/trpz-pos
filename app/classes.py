from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Time, Float, Table, DateTime
from sqlalchemy.orm import relationship
import sqlalchemy.ext
import sqlalchemy.ext.declarative

__all__ = ['User', 'Division', 'Till', 'Currency', 'DBBase',
           'Exchange', 'Report', 'Check', 'Availability', 'Ratio']

db = None
rd = None


DBBase = sqlalchemy.ext.declarative.declarative_base()


class User(DBBase):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    login = Column(String, nullable=False)
    phone = Column(String, nullable=False)
    email = Column(String, nullable=False)
    position = Column(String, nullable=False)
    password_hash = Column(String, nullable=False)
    password_salt = Column(String, nullable=False)

    reports = relationship('Report', back_populates='user')

    def to_json(self):
        return {field: self.__dict__[field] for field in ('id', 'name',
                                                          'login', 'email', 'position')}


class Availability(DBBase):
    __tablename__ = 'availability'

    id = Column(Integer, primary_key=True)
    division_id = Column(Integer, ForeignKey('divisions.id'), nullable=False)
    currency_id = Column(Integer, ForeignKey('currencies.id'), nullable=False)
    amount = Column(Float, nullable=False)

    division = relationship('Division')
    currency = relationship('Currency')


class Division(DBBase):
    __tablename__ = 'divisions'

    id = Column(Integer, primary_key=True)
    address = Column(String, nullable=False)
    head_id = Column(Integer, ForeignKey('users.id'), nullable=False)

    head = relationship('User')

    def to_json(self):
        return {'id': self.id, 'address': self.address, 'head_id': self.head_id}


class Till(DBBase):
    __tablename__ = 'tills'

    id = Column(Integer, primary_key=True)
    number = Column(Integer, nullable=False)
    division_id = Column(Integer, ForeignKey('divisions.id'), nullable=False)

    division = relationship('Division', back_populates='tills')

    def to_json(self):
        return {'id': self.id, 'number': self.number, 'division_id': self.division_id}
Division.tills = relationship('Till', back_populates='division')


class Currency(DBBase):
    __tablename__ = 'currencies'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    code = Column(String(3), nullable=False)

    def to_json(self):
        return {'id': self.id, 'name': self.name, 'code': self.code}

    def ratio_to(self, other):
        # perform query to get ratio
        pass


class Ratio(DBBase):
    __tablename__ = 'ratios'

    id = Column(Integer, primary_key=True)
    currency_buy = Column(Integer, ForeignKey('currencies.id'), nullable=False)
    currency_sell = Column(Integer, ForeignKey('currencies.id'), nullable=False)
    ratio = Column(Float, nullable=False)


class Exchange(DBBase):
    __tablename__ = 'exchanges'

    id = Column(Integer, primary_key=True)
    time = Column(DateTime, nullable=False)
    currency_buy = Column(Integer, ForeignKey('currencies.id'), nullable=False)
    currency_sell = Column(Integer, ForeignKey('currencies.id'), nullable=False)
    client_amount = Column(Float, nullable=False)
    till_id = Column(Integer, ForeignKey('tills.id'), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    exchanger_amount = Column(Float, nullable=False)

    till = relationship('Till')
    user = relationship('User')

    def to_json(self):
        return {'time': self.time.strftime('%Y-%m-%d %H:%M:%S'), 'currency_sell': self.currency_sell,
                'currency_buy': self.currency_buy, 'client_amount': self.client_amount,
                'exchanger_amount': self.exchanger_amount, 'till_id': self.till_id, 'user_id': self.user_id}


class Report(DBBase):
    __tablename__ = 'reports'

    id = Column(Integer, primary_key=True)
    division_id = Column(Integer, ForeignKey('divisions.id'), nullable=False)
    start_time = Column(DateTime, nullable=False)
    end_time = Column(DateTime, nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    content = Column(String)

    division = relationship('Division')
    user = relationship('User', back_populates='reports')

    def to_json(self):
        return {'id': self.id, 'division_id': self.division_id, 'start_time': self.start_time,
                'end_time': self.end_time, 'user_id': self.user_id, 'content': self.content}


class Check(DBBase):
    __tablename__ = 'checks'

    id = Column(Integer, primary_key=True)
    exchange_id = Column(Integer, ForeignKey('exchanges.id'), nullable=False)
    text = Column(String)

    exchange = relationship('Exchange')

    def to_json(self):
        return {'id': self.id, 'exchange': self.exchange.to_json(),
                'text': self.text}
