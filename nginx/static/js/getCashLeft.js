var itm = document.getElementById("curr0");
var e = document.getElementById("cash-left-list");
var curr_left;
var rowNum;

async function start() {
    const response = await fetch("api/get_cash_left")
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    console.log(JSON.stringify(data));
    return data;
}
start().then(data => main(data));



function main(data){
    curr_left = data;
    rowNum = curr_left.length;
    var childList = e.lastElementChild.childNodes;

    childList[1].lastElementChild.innerHTML = curr_left[0]["code"];
    childList[3].lastElementChild.innerHTML = curr_left[0]["name"];
    childList[5].lastElementChild.innerHTML = curr_left[0]["amount"].toFixed(2);
    


    for(let i = 1; i < rowNum; i++){
        var cln = itm.cloneNode(true);
        document.getElementById("cash-left-list").appendChild(cln);
        childList = e.lastElementChild.childNodes
        childList[1].lastElementChild.innerHTML = curr_left[i]["code"];
        childList[3].lastElementChild.innerHTML = curr_left[i]["name"];
        childList[5].lastElementChild.innerHTML = curr_left[i]["amount"].toFixed(2);        
    }

}

function updateList(){
    var child = e.lastElementChild;
    rowNum = curr_left.length;
    for(let i = 0; i < rowNum; i++){
        e.removeChild(child); 
        child = e.lastElementChild;
    }
    

    for(let i = 0; i < rowNum; i++){
        var cln = itm.cloneNode(true);
        document.getElementById("cash-left-list").appendChild(cln);
        childList = e.lastElementChild.childNodes
        childList[1].lastElementChild.innerHTML = curr_left[i]["code"];
        childList[3].lastElementChild.innerHTML = curr_left[i]["name"];
        childList[5].lastElementChild.innerHTML = curr_left[i]["amount"].toFixed(2);
    }

}
