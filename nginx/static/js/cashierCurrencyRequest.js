/*const dict={
    "names":
    [
        {
            "name":"Доллар США",
            "code":"USD"
        },
        {
            "name":"Евро",
            "code":"EUR"
        }
    ],
    "rates":
    [
        {
            "buy":"EUR",
            "sell":"USD",
            "rate":"1.111"
        },
        {
            "buy":"USD",
            "sell":"EUR",
            "rate":"0.900"
        }
    ]
}*/

const buyValue = document.getElementById('buy-value'),
    sellValue = document.getElementById('sell-value');
var selectBuy = document.getElementById("currency-buy");
var selectSell = document.getElementById("currency-sell");

var exchange_rate, curr_data;


async function start() {
    const response = await fetch('api/get_currencies')
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    return data;
}
start().then(data => main(data));

function main(data){
    curr_data = data;
    
    var itmB = document.getElementById("option-buy");
    var clnBuy, clnSell;
    var itmS = document.getElementById("option-sell");
    
    
    for (let i = 0; i < curr_data.names.length; i++){
        clnBuy = itmB.cloneNode(true);
        clnSell = itmS.cloneNode(true);
        clnBuy.removeAttribute('selected'); 
        clnSell.removeAttribute('selected');
        clnBuy.disabled = clnSell.disabled = false;
        clnBuy.value = clnSell.value = curr_data.names[i].code;
        clnBuy.innerHTML = clnSell.innerHTML = curr_data.names[i].name;
        console.log(curr_data.names[i].code)

        selectBuy.appendChild(clnBuy);
        selectSell.appendChild(clnSell);

    }

    
    

}

function getCurr(){
    var e = document.getElementById("currency-buy")
    var currBuy = e.options[e.selectedIndex].value;
    e = document.getElementById("currency-sell")
    var currSell = e.options[e.selectedIndex].value;

    sellValue.value = '';

    for(let i = 0; i < curr_data.rates.length; i++){
        if(curr_data.rates[i].buy == currBuy && curr_data.rates[i].sell == currSell){
                exchange_rate = curr_data.rates[i].rate;
                console.log(currBuy, currSell, exchange_rate);
                var result = parseFloat(buyValue.value) * exchange_rate;
                sellValue.value = !isNaN(result) ? result.toFixed(2) : '';
                break;
        }
    }
}



//api/add_exchangebuy=USD&sell=EUR&amount=10

async function pushExchange() {
    const currBuyVal = selectBuy.value;
    const currSellVal = selectSell.value;
    const amountBuy = document.getElementById("buy-value").value;
    var url = "api/add_exchange?buy=" + currBuyVal + "&sell=" + currSellVal + "&amount=" + amountBuy;
    const response = await fetch(url);
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    console.log(JSON.stringify(data));
    return data;
}

