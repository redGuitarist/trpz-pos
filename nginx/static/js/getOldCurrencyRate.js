/*const dict={
    "names":
    [
        {
            "name":"Доллар США",
            "code":"USD"
        },
        {
            "name":"Евро",
            "code":"EUR"
        }
    ],
    "rates":
    [
        {
            "buy":"EUR",
            "sell":"USD",
            "rate":"1.111"
        },
        {
            "buy":"USD",
            "sell":"EUR",
            "rate":"0.900"
        }
    ]
}*/

var curr_data;
var currBuy, currSell;

async function start() {
    const response = await fetch('api/get_currencies')
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    console.log(JSON.stringify(data));
    return data;
}
start().then(data => main(data));

function main(data){
    curr_data = data;
        
    var itmB = document.getElementById("option-buy");
    var clnBuy, clnSell;
    var itmS = document.getElementById("option-sell");
    var selectBuy = document.getElementById("currency-buy");
    var selectSell = document.getElementById("currency-sell");
    
    for (let i = 0; i < curr_data.names.length; i++){
        clnBuy = itmB.cloneNode(true);
        clnSell = itmS.cloneNode(true);
        clnBuy.selected = clnSell.selected = false;
        clnBuy.disabled = clnSell.disabled = false;
        clnBuy.value = clnSell.value = curr_data.names[i].code;
        clnBuy.innerHTML = clnSell.innerHTML = curr_data.names[i].name;
        console.log(curr_data.names[i].code)

        selectBuy.appendChild(clnBuy);
        selectSell.appendChild(clnSell);

    }

}

console.log(JSON.stringify(curr_data)); 


function getCurr(){
    var e = document.getElementById("currency-buy")
    currBuy = e.options[e.selectedIndex].value;
    e = document.getElementById("currency-sell")
    currSell = e.options[e.selectedIndex].value;

    document.getElementById('old-value').value = '';
    document.getElementById('new-value').readOnly = true;

    for(let i = 0; i < curr_data.rates.length; i++){
        if(curr_data.rates[i].buy == currBuy && curr_data.rates[i].sell == currSell){
            document.getElementById('old-value').value = curr_data.rates[i].rate;
            document.getElementById('new-value').readOnly = false;
            break;
        }
    }
}



//http://localhost/api/set_exchange_rate?buy=USD&sell=EUR&rate=0.9


async function setRate() {
    const currBuyVal = document.getElementById("currency-buy").value;
    const currSellVal = document.getElementById("currency-sell").value;
    const newRateVal = document.getElementById("new-value").value;
    var url = "api/set_exchange_rate?buy=" + currBuyVal + "&sell=" + currSellVal + "&rate=" + newRateVal;
    const response = await fetch(url);
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    console.log(JSON.stringify(data));
    return data;
}

