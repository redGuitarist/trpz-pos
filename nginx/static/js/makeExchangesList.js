/*const dict={

    [
        {
            "ID":"1",
            "timestamp":"30oct2019 19:00",//??
            "buy":"USD",
            "buyValue":"100",
            "sell":"EUR",
            "sellValue":"90"
        },
        {
            "ID":"2",
            "timestamp":"30oct2019 19:05",//??
            "buy":"EUR",
            "buyValue":"100",
            "sell":"USD",
            "sellValue":"111.1"
        },
    ],
}*/

var itm = document.getElementById("exchange0");
var selectNum = document.getElementById("choose-exchanges-num");
var e = document.getElementById("last-exchanges-list");
var exchanges_data;
var rowNum;

async function start() {
    const response = await fetch("api/get_exchanges?limit=5")
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    console.log(JSON.stringify(data));
    return data;
}
start().then(data => main(data));



function main(data){
    exchanges_data = data;
    rowNum = Math.min(exchanges_data.length, selectNum.options[selectNum.selectedIndex].value);
    var childList = e.lastElementChild.childNodes;

    childList[1].lastElementChild.innerHTML = exchanges_data[0]["till_id"];
    childList[3].lastElementChild.innerHTML = exchanges_data[0]["time"];
    childList[7].lastElementChild.innerHTML = exchanges_data[0]["exchanger_amount"] + " " + exchanges_data[0]["currency_buy"];
    childList[5].lastElementChild.innerHTML = exchanges_data[0]["client_amount"] + " " + exchanges_data[0]["currency_sell"];


    for(let i = 1; i < rowNum; i++){
        var cln = itm.cloneNode(true);
        document.getElementById("last-exchanges-list").appendChild(cln);
        childList = e.lastElementChild.childNodes
        childList[1].lastElementChild.innerHTML = exchanges_data[i]["till_id"];
        childList[3].lastElementChild.innerHTML = exchanges_data[i]["time"];
        childList[7].lastElementChild.innerHTML = exchanges_data[i]["exchanger_amount"] + " " + exchanges_data[0]["currency_buy"];
        childList[5].lastElementChild.innerHTML = exchanges_data[i]["client_amount"] + " " + exchanges_data[0]["currency_sell"];
        
    }

}

function updateList(){
    var child = e.lastElementChild;
    
    for(let i = 1; i < rowNum; i++){
        e.removeChild(child); 
        child = e.lastElementChild;
    }
    rowNum = Math.min(exchanges_data.length, selectNum.options[selectNum.selectedIndex].value);
    var childList = e.lastElementChild.childNodes;

    childList[1].lastElementChild.innerHTML = exchanges_data[0]["till_id"];
    childList[3].lastElementChild.innerHTML = exchanges_data[0]["time"];
    childList[7].lastElementChild.innerHTML = exchanges_data[0]["exchanger_amount"] + " " + exchanges_data[0]["currency_buy"];
    childList[5].lastElementChild.innerHTML = exchanges_data[0]["client_amount"] + " " + exchanges_data[0]["currency_sell"];
    

    for(let i = 1; i < rowNum; i++){
        var cln = itm.cloneNode(true);
        document.getElementById("last-exchanges-list").appendChild(cln);
        childList = e.lastElementChild.childNodes
        childList[1].lastElementChild.innerHTML = exchanges_data[i]["till_id"];
        childList[3].lastElementChild.innerHTML = exchanges_data[i]["time"];
        childList[7].lastElementChild.innerHTML = exchanges_data[i]["exchanger_amount"] + " " + exchanges_data[i]["currency_buy"];
        childList[5].lastElementChild.innerHTML = exchanges_data[i]["client_amount"] + " " + exchanges_data[i]["currency_sell"];
    }

}
