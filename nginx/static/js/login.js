//api/login?division=1&till=1&login=testuser&password=qwerty

async function login() {
    const division = document.getElementById("division").value;
    const till = document.getElementById("till").value;
    const userlogin = document.getElementById("userlogin").value;
    const userpassword = document.getElementById("userpassword").value;
    var url = "api/login?division=" + division + "&till=" + till + "&login=" + userlogin + "&password=" + userpassword;
    const response = await fetch(url);
    const json_curr_request = await response.json();
    console.log(JSON.stringify(json_curr_request));
    if (json_curr_request.success == true)
        var data = json_curr_request.data;
    else
        console.log(json_curr_request.error);
    console.log(JSON.stringify(data));
    
    if (json_curr_request.success == true)
        window.location.href = 'index.html';
}